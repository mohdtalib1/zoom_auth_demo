const express = require("express");
const request = require("request");
const bodyParser = require("body-parser");
const axios = require("axios");
const alert = require("alert");
require('dotenv').config();
const app = express();

app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

app.get("/", function (req, res) {
  res.render("login");
});


app.get("/scheduled_meetings/:token", function (req, res) {
  const headers = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${req.params.token}`,
  };

  axios
    .get("https://api.zoom.us/v2/users/me/meetings", {
      headers: headers,
    })
    .then((response) => {
      var meetingsArray = [];
      meetingsArray = response.data.meetings;

      res.render("scheduled_meetings", {
        Meetings: meetingsArray
      });
    })
    .catch((error) => {
      res.render("scheduled_meetings", {Meetings: meetingsArray});
    });

});

app.get("/recordings/:token", function (req, res) {
  const headers = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${req.params.token}`,
  };

  axios
    .get("https://api.zoom.us/v2/users/me/recordings?from=2021-01-01", {
      headers: headers,
    })
    .then((response) => {
      var recordingsArray = [];
      recordingsArray = response.data.meetings;

      res.render("recordings", {
        from: response.data.from,
        to: response.data.to,
        total: response.data.total_records,
        Recordings: recordingsArray
      });
    })
    .catch((error) => {
      res.render("recordings", { from: "Error", to: "Error", total: "Error" });
    });
});

app.get("/recordings_demo/:token", function (req, res) {
  const headers = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${req.params.token}`,
  };

  // axios
  //   .get("https://api.zoom.us/v2/users/me/recordings?from=2021-01-01", {
  //     headers: headers,
  //   })
    // .then((response) => {
      var recordingsArray = [];
    //   recordingsArray = response.data.meetings;

      var meetingsOBJ1 =  {
        "uuid": "Uwf1QS+4Tiahm7VZCM2yKw==",
        "id": 86507353645,
        "account_id": "QrYKMqaXSCGwjOCEdN_Fdg",
        "host_id": "CyBmFwBxSl62hb8Zk_5sJA",
        "topic": "Turner",
        "type": 3,
        "start_time": "2022-01-31T09:27:34Z",
        "timezone": "Asia/Calcutta",
        "duration": 10,
        "total_size": 37867454,
        "recording_count": 2,
        "share_url": "https://us02web.zoom.us/rec/share/nix5CJsSZUFZ8Kqf8SEyvi-Il54RH86tJBu8rxN_0chE7e3S6a6TwprXZGM4VELP.DN0MlbJYaUvDGbUZ",
        "recording_files": [
          {
            "id": "3327051f-d018-45c6-9b18-e170265246c8",
            "meeting_id": "Uwf1QS+4Tiahm7VZCM2yKw==",
            "recording_start": "2022-01-31T09:28:51Z",
            "recording_end": "2022-01-31T09:39:37Z",
            "file_type": "MP4",
            "file_extension": "MP4",
            "file_size": 27583950,
            "play_url": "https://us02web.zoom.us/rec/play/xWk0V7LcVs0tpXMjs3PFK2gC0F4uVPD6XHYBWlTiIExfTQ8g5fUdXTqXZq4XPO2FkaUU9EbY6ghxCfbq.gWwbaAFKGSp9H70z",
            "download_url": "https://us02web.zoom.us/rec/download/xWk0V7LcVs0tpXMjs3PFK2gC0F4uVPD6XHYBWlTiIExfTQ8g5fUdXTqXZq4XPO2FkaUU9EbY6ghxCfbq.gWwbaAFKGSp9H70z",
            "status": "completed",
            "recording_type": "shared_screen_with_speaker_view"
          },
          {
            "id": "9ddbc7f0-6349-4d3b-b75f-73a6f88fffe9",
            "meeting_id": "Uwf1QS+4Tiahm7VZCM2yKw==",
            "recording_start": "2022-01-31T09:28:51Z",
            "recording_end": "2022-01-31T09:39:37Z",
            "file_type": "M4A",
            "file_extension": "M4A",
            "file_size": 10283504,
            "play_url": "https://us02web.zoom.us/rec/play/4thX62j7xfkzL-ocAxlx5EVC4AUD50CiWIvwyqVmornSs5VdhkIB4Dd6CYJp0hNZNYC8ysOIieKZNqpv.exkrkbYK8sz7a4xo",
            "download_url": "https://us02web.zoom.us/rec/download/4thX62j7xfkzL-ocAxlx5EVC4AUD50CiWIvwyqVmornSs5VdhkIB4Dd6CYJp0hNZNYC8ysOIieKZNqpv.exkrkbYK8sz7a4xo",
            "status": "completed",
            "recording_type": "audio_only"
          }
        ]
      };

      var meetingsOBJ2 =   {
        "uuid": "Dt12faLBTkeIxXuod7D9Pw==",
        "id": 86507353645,
        "account_id": "QrYKMqaXSCGwjOCEdN_Fdg",
        "host_id": "CyBmFwBxSl62hb8Zk_5sJA",
        "topic": "Turner",
        "type": 3,
        "start_time": "2022-01-28T09:40:32Z",
        "timezone": "Asia/Calcutta",
        "duration": 36,
        "total_size": 170261282,
        "recording_count": 2,
        "share_url": "https://us02web.zoom.us/rec/share/9NEMhXC9oPtRbDnprWE0sYplPJbIZAs2SvzHvo2mL7-gZosh6xgxN8cX89WGPyHq.2nLTynWcyA_ioUj8",
        "recording_files": [
          {
            "id": "6bc3ab2d-d456-4c2a-8c26-d036575f4968",
            "meeting_id": "Dt12faLBTkeIxXuod7D9Pw==",
            "recording_start": "2022-01-28T09:44:36Z",
            "recording_end": "2022-01-28T10:31:40Z",
            "file_type": "M4A",
            "file_extension": "M4A",
            "file_size": 35287749,
            "play_url": "https://us02web.zoom.us/rec/play/UCMcLwy5GfORWt-bjrLA0SxJI6d_eJgeN1ulXBRfMsn89xLowCxTc7kIvUk2ai9_S4P9yDADRz4D1eA_.slj3sVxPAMxxkxeq",
            "download_url": "https://us02web.zoom.us/rec/download/UCMcLwy5GfORWt-bjrLA0SxJI6d_eJgeN1ulXBRfMsn89xLowCxTc7kIvUk2ai9_S4P9yDADRz4D1eA_.slj3sVxPAMxxkxeq",
            "status": "completed",
            "recording_type": "audio_only"
          },
          {
            "id": "7e0e31ac-0524-460e-a612-8f3eab03dd0c",
            "meeting_id": "Dt12faLBTkeIxXuod7D9Pw==",
            "recording_start": "2022-01-28T09:44:36Z",
            "recording_end": "2022-01-28T10:31:40Z",
            "file_type": "MP4",
            "file_extension": "MP4",
            "file_size": 134973533,
            "play_url": "https://us02web.zoom.us/rec/play/a8VOg886pn_KJA1LmzMrPIoh6Xmp9aJ97W7S9Cypm5z8ui3rMUoCz_7qlntKc8Di_ZM1pFVOxEAki3u9.X0v_mwbPnZVeQvzx",
            "download_url": "https://us02web.zoom.us/rec/download/a8VOg886pn_KJA1LmzMrPIoh6Xmp9aJ97W7S9Cypm5z8ui3rMUoCz_7qlntKc8Di_ZM1pFVOxEAki3u9.X0v_mwbPnZVeQvzx",
            "status": "completed",
            "recording_type": "shared_screen_with_speaker_view"
          }
        ]
      };

      var meetingsOBJ3 =   
      {
        "uuid": "LBg3h6veRVmiPTF0BkHFCA==",
        "id": 86507353645,
        "account_id": "QrYKMqaXSCGwjOCEdN_Fdg",
        "host_id": "CyBmFwBxSl62hb8Zk_5sJA",
        "topic": "Turner",
        "type": 3,
        "start_time": "2022-01-27T14:41:19Z",
        "timezone": "Asia/Calcutta",
        "duration": 61,
        "total_size": 190280620,
        "recording_count": 2,
        "share_url": "https://us02web.zoom.us/rec/share/AwwXrGaWqgmQrlq_NBkCzaCwB8ZBNH7mdRMHllfWkScXxZHGWO2vDrODaLb2ux4.H9HVchfBucWVuOdD",
        "recording_files": [
          {
            "id": "bb23a4f6-88c1-49ec-8309-0ddf35baa209",
            "meeting_id": "LBg3h6veRVmiPTF0BkHFCA==",
            "recording_start": "2022-01-27T14:46:14Z",
            "recording_end": "2022-01-27T15:49:58Z",
            "file_type": "MP4",
            "file_extension": "MP4",
            "file_size": 131890795,
            "play_url": "https://us02web.zoom.us/rec/play/PS-9OvuFhsv-BK9SQw7cUPBOxfjvezM3adcSuLoEOz4RNO8QSUMbrmDdjuq_BsF08IsSsvqCR3h04Q_m.6Z6MR-aFyLnjZ0_g",
            "download_url": "https://us02web.zoom.us/rec/download/PS-9OvuFhsv-BK9SQw7cUPBOxfjvezM3adcSuLoEOz4RNO8QSUMbrmDdjuq_BsF08IsSsvqCR3h04Q_m.6Z6MR-aFyLnjZ0_g",
            "status": "completed",
            "recording_type": "shared_screen_with_speaker_view"
          },
          {
            "id": "f9cf1b6e-c5ef-4ab0-94ad-750a7ed4dbee",
            "meeting_id": "LBg3h6veRVmiPTF0BkHFCA==",
            "recording_start": "2022-01-27T14:46:14Z",
            "recording_end": "2022-01-27T15:49:58Z",
            "file_type": "M4A",
            "file_extension": "M4A",
            "file_size": 58389825,
            "play_url": "https://us02web.zoom.us/rec/play/VwOA31hqBiYto8rG5y7kDi1kgTP4xmlf3qZbvXzXa-AV2O5RCohGXjnNhXSNj6dFdO31IFPrNDxnuIRH.9UDx0TOa030_LyaQ",
            "download_url": "https://us02web.zoom.us/rec/download/VwOA31hqBiYto8rG5y7kDi1kgTP4xmlf3qZbvXzXa-AV2O5RCohGXjnNhXSNj6dFdO31IFPrNDxnuIRH.9UDx0TOa030_LyaQ",
            "status": "completed",
            "recording_type": "audio_only"
          }
        ]
      };

      var meetingsOBJ4 =  {
        "uuid": "37LqCThTRUO67ZCoqGSTkQ==",
        "id": 86507353645,
        "account_id": "QrYKMqaXSCGwjOCEdN_Fdg",
        "host_id": "CyBmFwBxSl62hb8Zk_5sJA",
        "topic": "Turner",
        "type": 3,
        "start_time": "2022-01-27T08:32:27Z",
        "timezone": "Asia/Calcutta",
        "duration": 101,
        "total_size": 480040206,
        "recording_count": 2,
        "share_url": "https://us02web.zoom.us/rec/share/Ko-a5G1Wm6G1C8z3UJT6infoMAmq9V2N5iIiL9FQXerXYBggAnZCc1BxF0cSVz7F.ZWZFzeHqXnMnO_pV",
        "recording_files": [
          {
            "id": "5ae7f24b-91ed-4619-aaf4-239e2217afa4",
            "meeting_id": "37LqCThTRUO67ZCoqGSTkQ==",
            "recording_start": "2022-01-27T08:36:18Z",
            "recording_end": "2022-01-27T10:39:36Z",
            "file_type": "MP4",
            "file_extension": "MP4",
            "file_size": 383155157,
            "play_url": "https://us02web.zoom.us/rec/play/Us8bnaDrKunV-nXqZf1Q-ehMgXsHFpfeaR5YyTOfpqpkxLBJSItEwsvx0pfzzIfvAjimfigIVBUSEFty.tfYyX_XFIbt81P9D",
            "download_url": "https://us02web.zoom.us/rec/download/Us8bnaDrKunV-nXqZf1Q-ehMgXsHFpfeaR5YyTOfpqpkxLBJSItEwsvx0pfzzIfvAjimfigIVBUSEFty.tfYyX_XFIbt81P9D",
            "status": "completed",
            "recording_type": "shared_screen_with_speaker_view"
          },
          {
            "id": "727a8dbf-0a90-4d47-8679-6114b2a942e9",
            "meeting_id": "37LqCThTRUO67ZCoqGSTkQ==",
            "recording_start": "2022-01-27T08:36:18Z",
            "recording_end": "2022-01-27T10:39:36Z",
            "file_type": "M4A",
            "file_extension": "M4A",
            "file_size": 96885049,
            "play_url": "https://us02web.zoom.us/rec/play/BkAQ9mKhzljZLkR1Fe2RVUXYZ0e3VqSwIRtVgKNiwQ9uGXtzlWxKp4etKMpPfMLt2Ujx5WdMxzdsW2yD.H0KcYjhpMdRdlFYe",
            "download_url": "https://us02web.zoom.us/rec/download/BkAQ9mKhzljZLkR1Fe2RVUXYZ0e3VqSwIRtVgKNiwQ9uGXtzlWxKp4etKMpPfMLt2Ujx5WdMxzdsW2yD.H0KcYjhpMdRdlFYe",
            "status": "completed",
            "recording_type": "audio_only"
          }
        ]
      };

      var meetingsOBJ5 =  {
        "uuid": "ooOGKZukSSS/kxEV4C89sQ==",
        "id": 86507353645,
        "account_id": "QrYKMqaXSCGwjOCEdN_Fdg",
        "host_id": "CyBmFwBxSl62hb8Zk_5sJA",
        "topic": "Turner",
        "type": 3,
        "start_time": "2022-01-24T09:06:46Z",
        "timezone": "Asia/Calcutta",
        "duration": 52,
        "total_size": 154697531,
        "recording_count": 3,
        "share_url": "https://us02web.zoom.us/rec/share/1CrfnJ0sYIRWtJKdYTMJhoZG7zZgPLPbPr0J9TgzEVMdUhkseXsYqgi_wAeS-UeU.7qkgi7FxEeIL3QAG",
        "recording_files": [
          {
            "id": "6567d567-030e-4d70-ae8d-3b68e4dbdfac",
            "meeting_id": "ooOGKZukSSS/kxEV4C89sQ==",
            "recording_start": "2022-01-24T14:34:56Z",
            "recording_end": "2022-01-24T15:27:40Z",
            "file_type": "CHAT",
            "file_extension": "TXT",
            "file_size": 107,
            "play_url": "https://us02web.zoom.us/rec/play/p--5MrZ9FhBd4aIbgZVMb1KQibk0ZGvRVATeEhiqOw89Mx87-Fbi1IzCXh3uqf9Q1ivzIxbjb_SVPfHv.9-7bbXivT1n6tAez",
            "download_url": "https://us02web.zoom.us/rec/download/p--5MrZ9FhBd4aIbgZVMb1KQibk0ZGvRVATeEhiqOw89Mx87-Fbi1IzCXh3uqf9Q1ivzIxbjb_SVPfHv.9-7bbXivT1n6tAez",
            "status": "completed",
            "recording_type": "chat_file"
          },
          {
            "id": "857d6889-8bc9-442e-b3cb-40ed919bf2d7",
            "meeting_id": "ooOGKZukSSS/kxEV4C89sQ==",
            "recording_start": "2022-01-24T14:34:56Z",
            "recording_end": "2022-01-24T15:27:40Z",
            "file_type": "M4A",
            "file_extension": "M4A",
            "file_size": 48828654,
            "play_url": "https://us02web.zoom.us/rec/play/5nIYfqW7jSDxVbZR_YtrB5mJEQhwIgi4e3zJdPFJROyhNx4obeNz15O434BBQgVOihbJpLQf0R5itfqm.4w3SF3MS-zrsTgWd",
            "download_url": "https://us02web.zoom.us/rec/download/5nIYfqW7jSDxVbZR_YtrB5mJEQhwIgi4e3zJdPFJROyhNx4obeNz15O434BBQgVOihbJpLQf0R5itfqm.4w3SF3MS-zrsTgWd",
            "status": "completed",
            "recording_type": "audio_only"
          },
          {
            "id": "f04c1655-eac0-462e-8d72-e872de55b599",
            "meeting_id": "ooOGKZukSSS/kxEV4C89sQ==",
            "recording_start": "2022-01-24T14:34:56Z",
            "recording_end": "2022-01-24T15:27:40Z",
            "file_type": "MP4",
            "file_extension": "MP4",
            "file_size": 105868770,
            "play_url": "https://us02web.zoom.us/rec/play/dm0acZ9I560E0l120JdFgmrxw4BpNa_2hhOA6igg1BOjyL468vKbAFFxuLKWrRTnRFn9FtbM30BaVXxq.f29v881kkZbOJspC",
            "download_url": "https://us02web.zoom.us/rec/download/dm0acZ9I560E0l120JdFgmrxw4BpNa_2hhOA6igg1BOjyL468vKbAFFxuLKWrRTnRFn9FtbM30BaVXxq.f29v881kkZbOJspC",
            "status": "completed",
            "recording_type": "shared_screen_with_speaker_view"
          }
        ]
      };

      var meetingsOBJ6 =  {
        "uuid": "Fe+KAfjkTf6PS/XzdUpLMQ==",
        "id": 89829519821,
        "account_id": "QrYKMqaXSCGwjOCEdN_Fdg",
        "host_id": "CyBmFwBxSl62hb8Zk_5sJA",
        "topic": "iConnect Edu",
        "type": 3,
        "start_time": "2022-01-19T11:14:40Z",
        "timezone": "Asia/Calcutta",
        "duration": 35,
        "total_size": 129755744,
        "recording_count": 2,
        "share_url": "https://us02web.zoom.us/rec/share/OWHFg_w2t8_uQftdFi6gAUMzLmd_1kGuvwPxL-xpQsxJLZgS_aQl_i_R01ZWGh8Z.45LhIhCJp7MBjI4a",
        "recording_files": [
          {
            "id": "3a36780a-a30a-4a7c-b4f5-c7137bcbd830",
            "meeting_id": "Fe+KAfjkTf6PS/XzdUpLMQ==",
            "recording_start": "2022-01-19T11:17:23Z",
            "recording_end": "2022-01-19T11:52:43Z",
            "file_type": "M4A",
            "file_extension": "M4A",
            "file_size": 33731933,
            "play_url": "https://us02web.zoom.us/rec/play/xpU4xR03F25hqFSDnunlVkt8rr-TPsnKWjeEVj94Xsej7gVxBDo55P_NvK2Y-9ce5e5vc3DA4EBs5C5U.t67hgdZdH7xTWMFB",
            "download_url": "https://us02web.zoom.us/rec/download/xpU4xR03F25hqFSDnunlVkt8rr-TPsnKWjeEVj94Xsej7gVxBDo55P_NvK2Y-9ce5e5vc3DA4EBs5C5U.t67hgdZdH7xTWMFB",
            "status": "completed",
            "recording_type": "audio_only"
          },
          {
            "id": "78058f26-3363-48be-a764-5f9f12c42625",
            "meeting_id": "Fe+KAfjkTf6PS/XzdUpLMQ==",
            "recording_start": "2022-01-19T11:17:23Z",
            "recording_end": "2022-01-19T11:52:43Z",
            "file_type": "MP4",
            "file_extension": "MP4",
            "file_size": 96023811,
            "play_url": "https://us02web.zoom.us/rec/play/RvP1EDBlX763aj4oOsUnBJXGeRNJ5zrnkJB2QKkW4hGF14Sqbhbvy-bXTHE0Gt0vegxe_o9YzK14wXF1.ZUmNJiqKojz224sD",
            "download_url": "https://us02web.zoom.us/rec/download/RvP1EDBlX763aj4oOsUnBJXGeRNJ5zrnkJB2QKkW4hGF14Sqbhbvy-bXTHE0Gt0vegxe_o9YzK14wXF1.ZUmNJiqKojz224sD",
            "status": "completed",
            "recording_type": "shared_screen_with_speaker_view"
          }
        ]
      };

      var meetingsOBJ7 =   {
        "uuid": "d/6CySDPRRCnWTlbcYc7DA==",
        "id": 86507353645,
        "account_id": "QrYKMqaXSCGwjOCEdN_Fdg",
        "host_id": "CyBmFwBxSl62hb8Zk_5sJA",
        "topic": "Turner",
        "type": 3,
        "start_time": "2022-01-17T12:49:21Z",
        "timezone": "Asia/Calcutta",
        "duration": 34,
        "total_size": 118579591,
        "recording_count": 2,
        "share_url": "https://us02web.zoom.us/rec/share/U_0bZlIa2Q_E13zWWuPtX_OkE2RRXG_yp_xNn4UdNgCokDLf0rj9uz43SLZXqmQ7.A4Bzv7wwIuvNqJoC",
        "recording_files": [
          {
            "id": "a3d33486-de79-4ef2-9a7d-dc750174c486",
            "meeting_id": "d/6CySDPRRCnWTlbcYc7DA==",
            "recording_start": "2022-01-17T14:46:14Z",
            "recording_end": "2022-01-17T15:21:07Z",
            "file_type": "MP4",
            "file_extension": "MP4",
            "file_size": 85283050,
            "play_url": "https://us02web.zoom.us/rec/play/WvomdLerUYzEsqNGwP6F_Sxczn6Pb_AAmwHQAV-BBxZMQWJkfVKQXK9xapvkTTXQIRkSXAVxf5vg6yRN.wIxqWuwU5H7YInVd",
            "download_url": "https://us02web.zoom.us/rec/download/WvomdLerUYzEsqNGwP6F_Sxczn6Pb_AAmwHQAV-BBxZMQWJkfVKQXK9xapvkTTXQIRkSXAVxf5vg6yRN.wIxqWuwU5H7YInVd",
            "status": "completed",
            "recording_type": "shared_screen_with_speaker_view"
          },
          {
            "id": "d23dda97-ed90-475b-af24-867484fe9adf",
            "meeting_id": "d/6CySDPRRCnWTlbcYc7DA==",
            "recording_start": "2022-01-17T14:46:14Z",
            "recording_end": "2022-01-17T15:21:07Z",
            "file_type": "M4A",
            "file_extension": "M4A",
            "file_size": 33296541,
            "play_url": "https://us02web.zoom.us/rec/play/MMIlHcp_ucqzRwSbBakHWDPd_qaHKD-lmUUB6aUBh-6umF_gtjLud1AlDU_vuW8IK-O2vjRL0DaFR9sA.KijG-ty5_Enod96R",
            "download_url": "https://us02web.zoom.us/rec/download/MMIlHcp_ucqzRwSbBakHWDPd_qaHKD-lmUUB6aUBh-6umF_gtjLud1AlDU_vuW8IK-O2vjRL0DaFR9sA.KijG-ty5_Enod96R",
            "status": "completed",
            "recording_type": "audio_only"
          }
        ]
      };

      recordingsArray.push(meetingsOBJ1);
      recordingsArray.push(meetingsOBJ2);
      recordingsArray.push(meetingsOBJ3);
      recordingsArray.push(meetingsOBJ4);
      recordingsArray.push(meetingsOBJ5);
      recordingsArray.push(meetingsOBJ6);
      recordingsArray.push(meetingsOBJ7);

      res.render("recordings_demo", {
        from: "44733",
        to: "39r8dgdd",
        total: 15,
        Recordings: recordingsArray
      });
    // })
    // .catch((error) => {
    //   res.render("recordings_demo", { from: "Error", to: "Error", total: "Error" });
    // });
});

app.get("/user_details/:token", function (req, res) {
  const headers = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${req.params.token}`,
  };

  axios
    .get("https://api.zoom.us/v2/users/me", {
      headers: headers,
    })
    .then((response) => {
      var fullName = response.data.first_name + " " + response.data.last_name;

      console.log(response.data);
      res.render("user_details", {
        userId: response.data.id,
        name: fullName,
        email: response.data.email,
        role: response.data.role_name,
        url: response.data.personal_meeting_url,
      });
    })
    .catch((error) => {
      res.render("user_details", {
        userId: "Error",
        name: "Error",
        email: "Error",
        role: "Error",
        url: "Error",
      });
      console.log(error);
    });
});

app.get("/token", function (req, res) {
  if (req.query.code) {
    let url =
      "https://zoom.us/oauth/token?grant_type=authorization_code&code=" +
      req.query.code +
      "&redirect_uri=https://zoomapidemo.herokuapp.com/token";

    request
      .post(url, (error, response, body) => {
        // Parse response to JSON
        body = JSON.parse(body);

        // Logs your access and refresh tokens in the browser
        console.log(`access_token: ${body.access_token}`);
        console.log(`refresh_token: ${body.refresh_token}`);

        if (body.access_token) {
          res.render("token", { token: body.access_token });
        } else {
          res.render("token", { token: "No Token" });
        }
      })
      .auth("WvgsfL7xSoWqd1ljj5j0Wg", "3A1B7lKQK6VW6JuBjrVmXQNlTSabJWRd");

    return;
  }

  // Step 2:
  // If no authorization code is available, redirect to Zoom OAuth to authorize
  res.redirect(
    "https://zoom.us/oauth/authorize?response_type=code&client_id=WvgsfL7xSoWqd1ljj5j0Wg&redirect_uri=https://zoomapidemo.herokuapp.com/token"
  );
});

app.post("/token", function (req, res) {
  res.status(301).redirect("https://zoom.us/logout");
});

app.listen(process.env.PORT || 3000, function (req, res) {
  console.log("Server is Running on 3000");
});
